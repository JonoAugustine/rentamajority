# Rent A Majority

rentamajority.com

A satirical webpage inspired by [rent-a-minority](http://rentaminority.com/), a site I've had bookmarked for years.

## Disclaimer

The content of Rent A Majority is satire and not to be taken literally.
